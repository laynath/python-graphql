import graphene
import json
from datetime import datetime
from uuid import uuid4


class Post(graphene.ObjectType):
    title = graphene.String()
    content = graphene.String()

class CreatePost(graphene.Mutation):

    post = graphene.Field(Post)

    class Arguments:
        title = graphene.String()
        content = graphene.String()
    
    def mutate(self, info, title, content):
        post = Post(title=title, content=content)
        return CreatePost(post = post)
        

class User(graphene.ObjectType):
    id = graphene.ID(default_value=str(uuid4()))
    username = graphene.String()
    created_at = graphene.DateTime(default_value=datetime.now())


class CreateUser(graphene.Mutation):
    user = graphene.Field(User)

    class Arguments:
        username = graphene.String()

    def mutate(self, info, username):
        user = User(username=username)
        return CreateUser(user = user)



class Mutation(graphene.ObjectType):
    create_user = CreateUser.Field()
    Create_post = CreatePost.Field()


class Query(graphene.ObjectType):
    hello = graphene.String()
    is_admin = graphene.Boolean()
    user = graphene.List(User, limit = graphene.Int())

    def resolve_hello(self, info):
        return "world"
    
    def resolve_is_admin(self, info):
        return True

    def resolve_user(self, info, limit= None):
        return [
            User(id="1", username= "Freya", created_at = datetime.now()),
            User(id="2", username= "Kratos", created_at = datetime.now())
        ][:limit]

schema = graphene.Schema(query=Query, mutation=Mutation)
# if you use auto_camelcase=Fale you use < is_admin >

result = schema.execute(
    '''
            mutation{
                CreatePost(title: "god of war", content: "war"){
                    post{
                        title
                        content
                    }
                }
            }    
    '''

)

dictresult = dict(result.data.items())

print(json.dumps(dictresult, indent=2))

# msipl627rc